package dstreams.record;

import dstreams.conversion.Converter;
import org.junit.Test;
import org.mutabilitydetector.unittesting.AllowedReason;
import dstreams.conversion.StringConverter;

import static org.mutabilitydetector.unittesting.AllowedReason.allowingForSubclassing;
import static org.mutabilitydetector.unittesting.AllowedReason.assumingFields;
import static org.mutabilitydetector.unittesting.AllowedReason.provided;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertImmutable;
import static org.mutabilitydetector.unittesting.MutabilityAssert.assertInstancesOf;
import static org.mutabilitydetector.unittesting.MutabilityMatchers.areImmutable;

/**
 *
 */
public class ImmutableTest {

    /**
     * Immutable if converters are immutable
     */
    @Test
    public void checkImmutabilityOfConvertRules() {
        assertInstancesOf(StringConverter.class,
                areImmutable(),
                provided(Converter.class).isAlsoImmutable(),
                assumingFields("byColumnName", "byType").areNotModifiedAndDoNotEscape(),
                assumingFields("byColumnName", "byType").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements());
    }

    @Test
    public void checkImmutabilityOfColumn() {
        assertImmutable(Column.class);
    }

	@Test
	public void checkImmutabilityOfDeifinition() {
		assertInstancesOf(Definition.class,
				areImmutable(),
				assumingFields("columns").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements(),
				assumingFields("columnNames", "columnsSet").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements(),
				assumingFields("columnNames", "columnsSet").areModifiedAsPartOfAnUnobservableCachingStrategy());
	}

    /**
     * Almost immutable - no defensive copy for values (performance)
     * Records loaded by Parser's are immutable
     * Possibility to be subclassed
     */
    @Test
    public void checkImmutabilityOfSimpleRecord() {
        assertInstancesOf(SimpleRecord.class,
				areImmutable(),
                provided(Definition.class).isAlsoImmutable(),
                allowingForSubclassing(),
                assumingFields("data").areNotModifiedAndDoNotEscape(),
                assumingFields("data").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements(),
				assumingFields("HASH_CODE").areModifiedAsPartOfAnUnobservableCachingStrategy());
    }

	@Test
	public void checkImmutabilityOfConvertedRecord() {
		assertInstancesOf(ConvertedRecord.class,
				areImmutable(),
				provided(Definition.class, Record.class).areAlsoImmutable(),
				allowingForSubclassing(),
				assumingFields("converters").areNotModifiedAndDoNotEscape(),
				assumingFields("converters").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements(),
				assumingFields("HASH_CODE", "CONVERTED_DEFINITION").areModifiedAsPartOfAnUnobservableCachingStrategy());
	}

	@Test
    public void checkImmutabilityOfOrder() {
        assertInstancesOf(Order.class,
                areImmutable(),
                AllowedReason.assumingFields("order", "keys").areSafelyCopiedUnmodifiableCollectionsWithImmutableElements());
    }
}
