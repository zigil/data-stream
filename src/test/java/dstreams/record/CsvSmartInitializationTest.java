package dstreams.record;

import com.google.common.collect.Lists;
import dstreams.streams.CsvParser;
import org.junit.Test;
import dstreams.streams.Source;

import static dstreams.record.Column.column;
import static org.assertj.core.api.Assertions.assertThat;

public class CsvSmartInitializationTest {
    @Test
    public void testParse() {
        Definition def = Definition.newDefinition(
				column("no.", Integer.class),
                column("namw", String.class),
                column("season", String.class));

        Iterable<Record> records = Lists.newArrayList(
                CsvParser.parseAll(Source.fromClassPath("months.txt"), def, "\t"));

        assertThat(records).contains(
                Record.Parsers.fromString("3,march,winter", ",", def));

        assertThat(records).contains(
                Record.Parsers.fromString("3, march, winter", ", ", def));

        assertThat(records).contains(
                Record.Parsers.fromString("3	march	winter", "\t", def));
    }
}
