package dstreams.record;

import dstreams.conversion.Converter;
import org.junit.Test;

import javax.annotation.Nullable;

import static org.assertj.core.api.Assertions.assertThat;

public class TransformationTest {

    private static Record record = Months.JANUARY;

    @Test
    public void shouldTransformTypeInDefinition() {
        Transformation transformation = new Transformation.TransformBuilder()
                .convert("no.", new Converter<Integer, String>(String.class) {
                    @Nullable
                    @Override
                    public String apply(@Nullable Integer input) {
                        return String.valueOf(input)+"!";
                    }
                })
                .create();

        Record transformedRecord = transformation.transform(record);

        assertThat(transformedRecord.getDefinition()).isNotEqualTo(record.getDefinition());
        assertThat(transformedRecord.getDefinition().getColumn("no.").getType()).isEqualTo(String.class);
        assertThat(transformedRecord.getValue("no.").getClass()).isEqualTo(String.class);
        assertThat(transformedRecord.getValue("no.")).isEqualTo("1!");
    }

    @Test
    public void shouldRenameColumn() {
        Transformation transformation = new Transformation.TransformBuilder()
                .changeColumnName("name", "newName")
                .create();

        Record transformedRecord = transformation.transform(record);

        assertThat(transformedRecord.getDefinition()).isNotEqualTo(record.getDefinition());
        assertThat(transformedRecord.getDefinition().getColumn("newName").getType()).isEqualTo(String.class);
        assertThat(transformedRecord.getDefinition().hasColumn("name")).isFalse();
        assertThat(transformedRecord.getValue("newName")).isEqualTo("january");
    }

    @Test
    public void shouldChangeColumnValue() {
        Transformation transformation = new Transformation.TransformBuilder()
                .changeValue("name", new Converter<RecordLike, String>(String.class) {
                    @Nullable
                    @Override
                    public String apply(@Nullable RecordLike input) {
                        return input.getValue("season").toString();
                    }
                })
                .create();

        Record transformedRecord = transformation.transform(record);

        assertThat(transformedRecord.getDefinition()).isEqualTo(record.getDefinition());
        assertThat(transformedRecord.getDefinition().getColumn("name").getType()).isEqualTo(String.class);
        assertThat(transformedRecord.getValue("name")).isEqualTo("winter");
    }

    @Test
    public void shouldChangeColumnValueAndType() {
        Transformation transformation = new Transformation.TransformBuilder()
                .changeValue("name", new Converter<RecordLike, Double>(Double.class) {
                    @Nullable
                    @Override
                    public Double apply(@Nullable RecordLike input) {
                        return 3.14d;
                    }
                })
                .create();

        Record transformedRecord = transformation.transform(record);

        assertThat(transformedRecord.getDefinition()).isNotEqualTo(record.getDefinition());
        assertThat(transformedRecord.getDefinition().getColumn("name").getType()).isEqualTo(Double.class);
        assertThat(transformedRecord.getValue("name")).isEqualTo(3.14d);
    }

    @Test
    public void shouldAddNewColumn() {
        Transformation transformation = new Transformation.TransformBuilder()
                .addColumn("nameAndNo", new Converter<RecordLike, String>(String.class) {
                    @Nullable
                    @Override
                    public String apply(@Nullable RecordLike input) {
                        return input.getValue("name").toString() + input.getValue("no.");
                    }
                })
                .create();

        Record transformedRecord = transformation.transform(record);

        assertThat(transformedRecord.getDefinition()).isNotEqualTo(record.getDefinition());
        assertThat(transformedRecord.getDefinition().getColumn("nameAndNo").getType()).isEqualTo(String.class);
        assertThat(transformedRecord.getValue("nameAndNo")).isEqualTo("january1");
    }
}
