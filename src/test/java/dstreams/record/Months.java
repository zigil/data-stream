package dstreams.record;

import org.joda.time.LocalDate;
import org.junit.Test;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

public class Months {
    public static Definition MONTHS = newDefinition(
            column("name", String.class),
            column("no.", Integer.class),
            column("season", String.class),
            column("begin", LocalDate.class));

    public static Definition SHORT_MONTHS = newDefinition(
            column("name", String.class),
            column("no.", Integer.class));

    public static Record JANUARY = new Record.Builder(MONTHS)
            .set("name", "january")
            .set("no.", 1)
            .set("season", "winter")
            .set("begin", new LocalDate(2014, 1, 1))
            .create();

    public static Record FEBRUARY = new Record.Builder(MONTHS)
            .set("name", "february")
            .set("no.", 2)
            .set("season", "winter")
            .set("begin", new LocalDate(2014, 2, 1))
            .create();

    public static Record APRIL = new Record.Builder(MONTHS)
            .set("name", "april")
            .set("no.", 4)
            .set("season", "spring")
            .set("begin", new LocalDate(2014, 4, 1))
            .create();

    @Test
    public void listDefinition() throws InterruptedException {
        System.out.println(MONTHS);
    }

    @Test
    public void listRecord() throws InterruptedException {
        System.out.println(JANUARY);
    }
}
