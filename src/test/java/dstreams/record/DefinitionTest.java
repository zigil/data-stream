package dstreams.record;

import org.javatuples.Pair;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

/**
 *
 */
public class DefinitionTest {

	@Test
	public void diffShouldDealWithEqualsDefinitions() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		Definition def2 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		assertThat(def1.diff(def2)).isEmpty();
	}

	@Test
	public void diffShouldFindDiffTypes() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", Integer.class));

		Definition def2 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		Map<String, Pair<Class, Class>> diff = def1.diff(def2);
		assertThat(diff)
				.isNotEmpty()
				.hasSize(1)
				.contains(entry("month", new Pair(Integer.class, String.class)));
	}

	@Test
	public void diffShouldFindExtraColumnInFirst() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", Integer.class),
				column("day", BigDecimal.class));

		Definition def2 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		Map<String, Pair<Class, Class>> diff = def1.diff(def2);
		assertThat(diff)
				.isNotEmpty()
				.hasSize(2)
				.contains(entry("month", new Pair(Integer.class, String.class)))
				.contains(entry("day", new Pair(BigDecimal.class, null)));
	}

	@Test
	public void diffShouldFindExtraColumnsInBoth() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", Integer.class),
				column("day", BigDecimal.class));

		Definition def2 = newDefinition(
				column("year", Date.class),
				column("month", String.class),
				column("Day", BigDecimal.class));

		Map<String, Pair<Class, Class>> diff = def1.diff(def2);
		assertThat(diff)
				.isNotEmpty()
				.hasSize(3)
				.contains(entry("month", new Pair(Integer.class, String.class)))
				.contains(entry("day", new Pair(BigDecimal.class, null)))
				.contains(entry("Day", new Pair(null, BigDecimal.class)));
	}

	@Test
	public void testToString() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		String asString = def1.toString();
		assertThat(asString).isEqualTo("Definition[year:java.util.Date, month:java.lang.String]");
	}

	@Test
	public void fromString() {
		Definition def1 = newDefinition(
				column("year", Date.class),
				column("month", String.class));

		assertThat(Definition.fromString("Definition[month:java.lang.String, year:java.util.Date]"))
				.isEqualTo(def1);

		assertThat(Definition.fromString("month:java.lang.String, year:java.util.Date"))
				.isEqualTo(def1);

		assertThat(Definition.fromString("[month:java.lang.String, year:java.util.Date]"))
				.isEqualTo(def1);
	}
}

