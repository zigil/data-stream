package dstreams.comparator;

import dstreams.record.Definition;
import dstreams.streams.CsvParser;
import org.junit.Test;
import dstreams.record.Order;
import dstreams.record.Record;
import dstreams.streams.Source;

import java.io.FileNotFoundException;

import static dstreams.record.Column.column;

public class ComparatorTest {

    @Test
    public void produceOutput() throws FileNotFoundException {
        Definition def = Definition.newDefinition(
				column("colA", String.class),
				column("colB", Integer.class),
				column("colC", String.class),
				column("colD", String.class));

        Order order = Order.asc("colB");

        Iterable<Record> stream1 = CsvParser.parseAllAndSort(Source.fromClassPath("str1.txt"), def, "\t", order);
        Iterable<Record> stream2 = CsvParser.parseAllAndSort(Source.fromClassPath("str2.txt"), def, "\t", order);

        new Comparator(".", stream1, stream2, order).compare();
    }
}
