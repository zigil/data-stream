package dstreams.db;

import dstreams.streams.CsvParser;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import dstreams.record.Column;
import dstreams.record.Definition;
import dstreams.record.Record;
import dstreams.streams.Source;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;
import static dstreams.record.Definition.newDefinitionStrings;
import static org.assertj.core.api.Assertions.assertThat;

public class DbConnectionTest {

	private static JdbcConnectionPool cp;
	public static final Definition DEFINITION_SHOULD_BE = newDefinition(
			column("id", Integer.class),
			column("name", String.class)
	);

	@BeforeClass
	public static void beforeClass() throws SQLException {
		cp = JdbcConnectionPool.create("jdbc:h2:mem:test", "sa", "sa");
		Connection con = cp.getConnection();

		Statement st = con.createStatement();
		st.executeUpdate("create table test (id int, name varchar(100))");

		st.executeUpdate("insert into test values(1, 'styczen')");
		st.executeUpdate("insert into test values(2, 'luty')");
		st.executeUpdate("insert into test values(3, 'marzec')");

		con.close();
	}

	@AfterClass
	public static void afterClass() {
		cp.dispose();
	}

	@Test
	public void testDefinition() throws SQLException {
		Connection con = cp.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select * from test where rownum<=1");

		Definition definition = Record.Parsers.definitionFromRuleSet(rs);
		con.close();

		assertThat(definition).isEqualTo(DEFINITION_SHOULD_BE);
	}

	@Test
	public void testParseRecordWithSchema() throws SQLException {
		Record shouldBe = new Record.Builder(DEFINITION_SHOULD_BE)
				.set("id", 1)
				.set("name", "styczen")
				.create();

		Connection con = cp.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select * from test order by id");
		rs.next();

		Definition definition = Record.Parsers.definitionFromRuleSet(rs);
		Record record = Record.Parsers.fromRuleSet(rs, definition);
		con.close();

		assertThat(record).isEqualTo(shouldBe);
	}

	@Test
	public void testParseRecord() throws SQLException {
		Definition defStrings = newDefinitionStrings("id", "name");

		Record shouldBe = new Record.Builder(defStrings)
				.set("id", "1")
				.set("name", "styczen")
				.create();

		Connection con = cp.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select * from test order by id");
		rs.next();

		Record record = Record.Parsers.fromRuleSet(rs, defStrings);
		con.close();

		assertThat(record).isEqualTo(shouldBe);
	}

	@Test
    public void testConnect() throws SQLException, ClassNotFoundException {
		Connection con = cp.getConnection();
		Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select * from test");

        ResultSetMetaData md = rs.getMetaData();
        int columnsCount = md.getColumnCount();
        List<Column> columns = new ArrayList<>();
        for (int i=1; i<=columnsCount; i++) {
            String label = md.getColumnLabel(i);
            String className = md.getColumnClassName(i);
            System.out.println(label+", "+className);

            columns.add(column(label, Class.forName(className)));
        }

        Definition definition = newDefinition(columns);
        System.out.println(definition);

        Definition defString = newDefinition(
                column("ID", Integer.class),
                column("NAME", String.class));

		Source source = Source.fromString(
				"1	styczen\r\n" +
						"2	luty\r\n" +
						"3	marzec\r\n");

		Iterable<Record> records = CsvParser.parseAll(source, defString, "\t");

        System.out.println("Do definitions are the same? " +
                records.iterator().next().getDefinition().equals(definition));
//        Comparator comparator = new Comparator()

        while (rs.next()) {
            Integer i = rs.getInt(1);
            String s = rs.getString(2);
            System.out.println(i+", "+s);
        }

        con.close();
    }
}
