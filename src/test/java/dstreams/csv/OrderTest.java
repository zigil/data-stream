//package dstreams.csv;
//
//import dstreams.streams.Source;
//import org.assertj.core.util.Lists;
//import org.joda.time.LocalDate;
//import org.junit.Test;
//import dstreams.record.Definition;
//import dstreams.record.Order;
//import dstreams.record.Record;
//import dstreams.comparator.Comparator;
//
//import java.io.FileNotFoundException;
//import java.util.Collections;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static dstreams.record.Column.*;
//import static dstreams.record.Definition.newDefinition;
//
//public class OrderTest {
//
//    private Definition definition = newDefinition(
//            column("no.", Integer.class),
//            column("name", String.class),
//            column("season", String.class),
//            column("begin", LocalDate.class));
//
//    private Mapper mapper = new Mapper.Builder()
//            .map(0, "no.")
//            .map(1, "name")
//            .map(2, "season")
//            .map(3, "begin")
//            .create();
//
//    private Mapper mapperDiffFormat = new Mapper.Builder()
//            .map(0, "begin")
//            .map(1, "no.")
//            .map(2, "name")
//            .map(3, "season")
//            .create();
//
//    public Iterable<Record> months = new CsvParser(definition, mapper).parse(
//            Source.fromClassPath("months.txt"));
//
//    public Iterable<Record> months3 = new CsvParser(definition, mapper).parse(
//            Source.fromClassPath("months3.txt"));
//
//    public Iterable<Record> monthsDiffFormat = new CsvParser(definition, mapperDiffFormat).parseAndSort(
//            Source.fromClassPath("months_different_format.txt"), Order.asc("no."));
//
//    @Test
//    public void testSortByName() {
//        Order order = new Order.Builder()
//                .keyAsc("name")
//                .create();
//
//        List<Record> monthsList = Lists.newArrayList(months);
//        Collections.sort(monthsList, order);
//
//        Record expecetdAsFisrt = new Record.Builder(definition)
//                .set("no.", 4)
//                .set("name", "april")
//                .set("season", "spring")
//                .set("begin", new LocalDate(2014, 4, 1))
//                .create();
//
//        assertThat(monthsList).startsWith(expecetdAsFisrt);
//    }
//
//    @Test
//    public void testCompareDifference() throws FileNotFoundException {
//        Order order = new Order.Builder()
//                .keyAsc("no.")
//                .create();
//
//        Comparator comparator = new Comparator(".", months.iterator(), months3.iterator(), order);
//        assertThat(comparator.compare()).isFalse();
//    }
//
//    @Test
//    public void testCompareEquals() throws FileNotFoundException {
//        Order order = new Order.Builder()
//                .keyAsc("no.")
//                .create();
//
//        Comparator comparator = new Comparator(".", months.iterator(), monthsDiffFormat.iterator(), order);
//        assertThat(comparator.compare()).isTrue();
//    }
//}
