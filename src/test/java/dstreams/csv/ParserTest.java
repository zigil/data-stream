//package dstreams.csv;
//
//import com.google.common.base.Function;
//import com.google.common.collect.Iterables;
//import com.google.common.collect.Lists;
//import dstreams.streams.Source;
//import org.joda.time.LocalDate;
//import org.junit.Test;
//import dstreams.record.Definition;
//import dstreams.record.Months;
//import dstreams.record.Order;
//import dstreams.record.Record;
//import dstreams.comparator.Comparator;
//
//import java.io.FileNotFoundException;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//public class ParserTest {
//
//    private static Definition DEFINITION = Months.MONTHS;
//
//    private static String TO_PARSE =
//            "1	january	winter	2014-01-01\r\n" +
//            "2	february	winter	2014-02-01\r\n" +
//            "3	march	winter	2014-03-01\r\n" +
//            "4	april	spring	2014-04-01\r\n" +
//			"5	may	spring	2014-05-01\r\n";
//
//    private static Mapper MAPPER = new Mapper.Builder()
//            .map(0, "no.")
//            .map(1, "name")
//            .map(2, "season")
//            .map(3, "begin")
//            .create();
//
//    CsvParser csvParser = new CsvParser(DEFINITION, MAPPER);
//
//    @Test
//    public void shouldParse() {
//        Iterable<Record> parsed = csvParser.parse(Source.fromString(TO_PARSE));
//        assertThat(parsed).contains(new Record.Builder(Months.MONTHS)
//                .set("no.", 3)
//                .set("name", "march")
//                .set("season", "winter")
//                .set("begin", new LocalDate(2014, 3, 1))
//                .create());
//    }
//
//    @Test
//    public void shouldConvert2Record() throws InterruptedException {
//        Record record = csvParser.newRecordFromStrings("2", "february", "winter", "2014-02-01");
//        assertThat(record).isEqualTo(new Record.Builder(Months.MONTHS)
//                .set("no.", 2)
//                .set("name", "february")
//                .set("season", "winter")
//                .set("begin", new LocalDate(2014, 2, 1))
//                .create());
//    }
//
//    @Test
//    public void shouldParseFromFile() {
//        Iterable<Record> parsed = csvParser.parse(Source.fromClassPath("months.txt"));
//        assertThat(parsed).contains(new Record.Builder(Months.MONTHS)
//                .set("no.", 3)
//                .set("name", "march")
//                .set("season", "winter")
//                .set("begin", new LocalDate(2014, 3, 1))
//                .create());
//    }
//
//    @Test
//    public void shouldCompareEqualsFiles() {
//        Iterable<Record> parsed = csvParser.parse(Source.fromClassPath("months.txt"));
//        Iterable<Record> parsed2 = csvParser.parse(Source.fromClassPath("months.txt"));
//
//        // convert to lists not to use lazy iterables...
//        assertThat(Lists.newArrayList(parsed))
//                .hasSameElementsAs(Lists.newArrayList(parsed2));
//    }
//
//    @Test
//    public void shouldCompareEqualsFilesButInDifferentSerialization() {
//        Mapper anotherMapping = new Mapper.Builder()
//                .map(1, "no.")
//                .map(2, "name")
//                .map(3, "season")
//                .map(0, "begin")
//                .create();
//
//        Iterable<Record> parsed = csvParser.parse(Source.fromClassPath("months.txt"));
//        Iterable<Record> parsed2 = new CsvParser(Months.MONTHS, anotherMapping).parse(Source.fromClassPath("months_different_format.txt"));
//
//        // convert to lists not to use lazy iterables...
//        assertThat(Lists.newArrayList(parsed))
//                .hasSameElementsAs(Lists.newArrayList(parsed2));
//    }
//
//    @Test
//    public void shouldCompareEqualsFileAndString() {
//        Iterable<Record> parsed = csvParser.parse(Source.fromClassPath("months.txt"));
//        Iterable<Record> parsed2 = csvParser.parse(Source.fromString(TO_PARSE));
//
//        // convert to lists not to use lazy iterables...
//        assertThat(Lists.newArrayList(parsed))
//                .hasSameElementsAs(Lists.newArrayList(parsed2));
//    }
//
//    @Test
//    public void shouldCompareNotEqualsFileAndString() {
//        Iterable<Record> parsed = csvParser.parse(Source.fromClassPath("months.txt"));
//        Iterable<Record> parsed2 = new CsvParser(Months.SHORT_MONTHS, MAPPER).parse(Source.fromString(TO_PARSE));
//
//        // convert to lists not to use lazy iterables...
//        assertThat(Lists.newArrayList(parsed))
//                .doesNotContainAnyElementsOf(Lists.newArrayList(parsed2));
//    }
//
//    @Test
//    public void shouldBeEqualAfterProjection() throws FileNotFoundException {
//        Iterable<Record> source = csvParser.parse(Source.fromClassPath("months.txt"));
//        Iterable<Record> dest = new CsvParser(Months.SHORT_MONTHS, MAPPER).parse(Source.fromString(TO_PARSE));
//
//        Order order = new Order.Builder().keyAsc("no.").create();
//
//        Comparator comparator = new Comparator(".", source.iterator(), dest.iterator(), order);
//        assertThat(comparator.compare()).isEqualTo(false);
//
//        Iterable<Record> projected = Iterables.transform(source, new Function<Record, Record>() {
//            @Override
//            public Record apply(Record input) {
//                return input.intersect(Months.SHORT_MONTHS.getColumnNames());
//            }
//        });
//
//        comparator = new Comparator(".", projected.iterator(), dest.iterator(), order);
//        assertThat(comparator.compare()).isEqualTo(true);
//    }
//
//}
