package dstreams.record;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;

@Immutable
@EqualsAndHashCode
@Getter
public class Column<T> implements Comparable<Column>, Serializable {
	private static final long serialVersionUID = 1L;

	private final String name;
    private final Class<T> type;

    private Column(String name, Class<T> type) {
        this.name = name;
        this.type = type;
    }

    public static <T> Column<T> column(String name, Class<T> type) {
        return new Column<>(name, type);
    }

    public String getName() {
        return name;
    }

    // for sorting
    @Override
    public int compareTo(Column o) {
        return this.name.compareTo(o.getName());
    }

	public String toString() {
		return String.format("Column[%s, %s]", name, type.getName());
	}

}
