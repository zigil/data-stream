package dstreams.record;

import dstreams.conversion.Converter;

import java.util.*;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

public class CalculatedRecord implements RecordLike {
    private static final long serialVersionUID = 1L;

    private final RecordLike baseRecord;
    private transient Definition calcDefinition;
    private final Map<String, Comparable> calcValues = new HashMap<>();

    CalculatedRecord(
            RecordLike record,
            Map<String, Converter<RecordLike, ? extends Comparable>> changers,
            Map<String, Converter<RecordLike, ? extends Comparable>> adders) {
        this.baseRecord = record;
        this.calcDefinition = calculateDefinition(record.getDefinition(), changers, adders);
    }

    @Override
    public Definition getDefinition() {
        return calcDefinition;
    }

    private Definition calculateDefinition(
            Definition def,
            Map<String, Converter<RecordLike, ? extends Comparable>> changers,
            Map<String, Converter<RecordLike, ? extends Comparable>> adders) {
        List<Column> newColumns = new ArrayList<>();

        for (Column c: def.getColumns()) {
            Converter<RecordLike, ? extends Comparable> changer = changers.get(c.getName());
            if (changer != null) {
                newColumns.add(column(c.getName(), changer.getTargetClass()));
                calcValues.put(c.getName(), changer.apply(baseRecord));
            }
            else {
                newColumns.add(c);
            }
        }

        for (Map.Entry<String, Converter<RecordLike, ? extends Comparable>> a: adders.entrySet()) {
            String name = a.getKey();
            Class<? extends Comparable> type = a.getValue().getTargetClass();

            newColumns.add(column(name, type));

            Comparable v = a.getValue().apply(baseRecord);
            calcValues.put(name, v);
        }

        return newDefinition(newColumns);
    }

    @Override
    public <T extends Comparable<T>> T getValue(String s) {
        Comparable calcValue = calcValues.get(s);
        if (calcValue != null) {
            return (T) calcValue;
        }

        return baseRecord.getValue(s);
    }

}
