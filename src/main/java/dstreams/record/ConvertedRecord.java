package dstreams.record;

import dstreams.conversion.Converter;

import java.util.*;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

/**
 * TODO: Consider do all calculations at once, not lazy - will optimize serialization!
 */
class ConvertedRecord implements RecordLike {
	private static final long serialVersionUID = 1L;

	private final Record baseRecord;
    private final Map<String, Converter> converters;
    private final Definition convDefinition;

    public ConvertedRecord(Record record, Map<String, Converter> converters) {
        this.baseRecord = record;
        this.converters = converters;
        this.convDefinition = convertDefinition(record.getDefinition(), converters);
    }

    private Definition convertDefinition(Definition def, Map<String, Converter> converters) {
        Collection<Column> columns = def.getColumns();
        List<Column> newColumns = new ArrayList<>();
        for (Column c: columns) {
            Converter converter = converters.get(c.getName());
            if (converter != null) {
                newColumns.add(column(c.getName(), converter.getTargetClass()));
            }
            else {
                newColumns.add(c);
            }
        }
        return newDefinition(newColumns);
    }

    @Override
    public Definition getDefinition() {
        return convDefinition;
    }

    @Override
    public <T extends Comparable<T>> T getValue(String s) {
        if (converters.get(s) != null) {
            return (T) converters.get(s).apply(baseRecord.getValue(s));
        }
        return (T) baseRecord.getValue(s);
    }

}
