package dstreams.record;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import dstreams.streams.Source;
import org.javatuples.Pair;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.*;

import static dstreams.record.Column.column;

/**
 * Definition of data. It's an ordered list of Columns.
 * Order allow to easily parse a String, however in comparison (equals, hashCode) the order doesn't matter
 *
 * TODO: Normalize...
 */
@Immutable
public final class Definition implements Serializable {
	private static final long serialVersionUID = 1L;

    private final List<Column> columns;

	// calculated field
	private transient Map<String, Column> columnNames;
	// calculated field only for fast equals and hashCode
	private transient Set<Column> columnsSet;

    private Definition(List<Column> columns) {
        this.columns = Collections.unmodifiableList(columns);
		init(columns);
    }

	// calculate fields
	private void init(List<Column> columns) {
		Map<String, Column> names = new HashMap<>();
		for (Column c: columns) {
			checkUniqueColumnNames(names, c);
			names.put(c.getName(), c);
		}
		this.columnNames = Collections.unmodifiableMap(names);
		this.columnsSet = Collections.unmodifiableSet(Sets.newHashSet(columns));
	}

	public static Definition newDefinition(List<Column> columns) {
		return new Definition(columns);
	}

    public static Definition newDefinition(Iterable<Column> columns) {
        return newDefinition(Lists.newArrayList(columns));
    }

    public static Definition newDefinition(Column... columns) {
		return newDefinition(Lists.newArrayList(columns));
    }

	/**
	 * All columns will be a java.lang.String type
	 */
	public static Definition newDefinitionStrings(Iterable<String> columnNames) {
		List<Column> columns = new ArrayList<>();
		for (String c: columnNames) {
			columns.add(column(c, String.class));
		}
		return newDefinition(columns);
	}

	/**
	 * All columns will be a java.lang.String type
	 */
	public static Definition newDefinitionStrings(String... columnNames) {
		List<Column> columns = new ArrayList<>();
		for (String c: columnNames) {
			columns.add(column(c, String.class));
		}
		return newDefinition(columns);
	}

	private void checkUniqueColumnNames(Map<String, Column> names, Column c) {
        Preconditions.checkArgument(! names.containsKey(c.getName()), c);
    }

	public boolean isEmpty() {
		return columns.isEmpty();
	}

	public List<Column> getColumns() {
		return columns;
	}

    public Column<?> getColumn(String c) {
        return columnNames.get(c);
    }

    public boolean hasColumn(String c) {
        return columnNames.containsKey(c);
    }

    public String getColumnName(String s) {
        return getColumn(s).getName();
    }

    public Collection<String> getColumnNames() {
        return columnNames.keySet();
    }

	public String toStringSorted() {
		// sort for readability
		StringBuilder sb = new StringBuilder("Definition[");
		List<Column> cols = new ArrayList<>(getColumns());
		Collections.sort(cols);

		for (Column c: cols) {
			sb.append(c.getName()).append(":").append(c.getType().getName()).append(", ");
		}
		sb.setCharAt(sb.length() - 2, ']');
		return sb.toString().trim();
	}
		@Override
    public String toString() {
        // sort for readability
        StringBuilder sb = new StringBuilder("Definition[");
        for (Column c: getColumns()) {
            sb.append(c.getName()).append(":").append(c.getType().getName()).append(", ");
        }
        sb.setCharAt(sb.length() - 2, ']');
        return sb.toString().trim();
    }

	public static Definition fromString(Source source) {
		Iterator<String> lines = source.getLines().iterator();

		if (! lines.hasNext()) {
			throw new RuntimeException("Empty source");
		}

		String line = lines.next();
		return fromString(line);
	}

	public static Definition fromString(String str) {
		str = str.replaceFirst("Definition", "");
		if (str.startsWith("[")) {
			str = str.substring(1, str.length());
		}
		if (str.endsWith("]")) {
			str = str.substring(0, str.length()-1);
		}

		List<Column> columns = new ArrayList<>();
		String[] split = str.split(",\\s*");
		for (int i=0; i<split.length; i++) {
			String[] columnSplit = split[i].split(":");
			try {
				Class c = Class.forName(columnSplit[1]);
				columns.add(column(columnSplit[0], c));
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return newDefinition(columns);
	}

    public Definition intersect(Collection<String> cols) {
        List<Column> newCols = new ArrayList<>();
        for (Column c: this.columns) {
            if (cols.contains(c.getName())) {
                newCols.add(c);
            }
        }
        return new Definition(newCols);
    }

	public Definition intersect(String... cols) {
		return intersect(Lists.newArrayList(cols));
	}

    public Definition intersect(Definition def) {
        return intersect(def.getColumnNames());
    }

    public Definition minus(Collection<String> cols) {
        List<Column> newCols = new ArrayList<>();
        for (Column c: this.columns) {
            if (! cols.contains(c.getName())) {
                newCols.add(c);
            }
        }
        return new Definition(newCols);
    }

	public Definition minus(String... cols) {
		return minus(Lists.newArrayList(cols));
	}

	public Definition minus(Definition def) {
        return minus(def.getColumnNames());
    }

	/**
	 * Override column conversion for columns that exists in passed definition
	 */
    public Definition overrideTypes(Definition def) {
        List<Column> newCols = new ArrayList<>();

        for (Column c: this.columns) {
            Class type = c.getType();
            String name = c.getName();

            if (def.hasColumn(name)) {
                type = def.columnNames.get(name).getType();
            }

            newCols.add(column(name, type));
        }
        return new Definition(newCols);
    }

	/**
	 * Override column conversion for columns that exists in passed columns
	 */
	public Definition overrideTypes(Column... columns) {
		Definition defOverriding = newDefinition(columns);
		return overrideTypes(defOverriding);
	}

	/**
	 * Override all columns to String type
	 */
	public Definition overrideTypesToString() {
		List<String> names = new ArrayList<>();
		for (Column c: getColumns()) {
			names.add(c.getName());
		}
		return newDefinitionStrings(names);
	}

	/**
	 * Find differences between definitions
	 */
	public SortedMap<String, Pair<Class, Class>> diff(Definition another) {
		SortedMap<String, Pair<Class, Class>> result = new TreeMap<>();
		for (String c: getColumnNames()) {
			if (! another.hasColumn(c)) {
				result.put(c, new Pair<Class, Class>(getColumn(c).getType(), null));
			}
			else if (! getColumn(c).getType().equals(
					another.getColumn(c).getType())) {
				result.put(c, new Pair<Class, Class>(getColumn(c).getType(), another.getColumn(c).getType()));
			}
		}
		for (Column c: another.minus(this).getColumns()) {
			result.put(c.getName(), new Pair(null, c.getType()));
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		return columnsSet.equals(((Definition) o).columnsSet);
	}

	@Override
	public int hashCode() {
		return columnsSet.hashCode();
	}

	private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
		aInputStream.defaultReadObject();
		init(columns);
	}


}
