package dstreams.record;

import com.google.common.base.Function;
import dstreams.streams.CsvParser;
import dstreams.streams.DbParser;
import org.javatuples.Pair;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.*;

/**
 * Record abstraction
 * All subclasses should be serializable
 */
public interface Record extends RecordLike, Comparator<Record>, Serializable {
	// methods that could be implemented using 'core methods'
	Map<String, Comparable> getValues();

	Record intersect(Record another);
	Record intersect(Definition def);
    Record intersect(Collection<String> columns);
	Record intersect(String... columns);

	/**
	 * Create a record representing a key specified by order
	 */
	Record key(Order order);

	Record minus(Record another);
	Record minus(Definition def);
	Record minus(Collection<String> columns);
	Record minus(String... columns);

	/**
	 * Concatenate records: all new columns in another will (that doesn't exists in this) will be added
	 */
	Record concatenate(Record another);

	/**
	 * Transform particular columns at once
	 * Use Transformation.TransformBuilder to specify transformation
	 */
	Record transform(Transformation transf);

	/**
	 * Convert conversion to be compatible with a definition
	 * Similar to transformation, but instead of explicit passing converter,
	 * one use ConvertRules to automatically find the right one
	 * See ConversionRules
	 */
	Record convertTypes(Definition def);

	/**
	 * Convert conversion to a given conversion
	 * Similar to transformation, but instead of explicit passing converter,
	 * one use ConvertRules to automatically find the right one
	 * See ConversionRules
	 */
	Record convertTypes(Column... columns);

	/**
	 * All columns will be normalized:
	 * 		Data, Timestamp into LocalDate
	 * 		All number into BigDecimal, decimal place cut to minimal (-2.00 -> -2, 3.1400 -> 3.14)
	 * 		String remains String
	 * 		All others into String
	 */
	Record normalize();

	SortedMap<String, Pair<Object, Object>> diff(Record another);
	SortedMap<String, Pair<Class, Class>> diffDefinition(Record another);

	String toStringCsv();

	// just notification that there will be special implementation
	String toString();
	boolean equals(Object o);
	int hashCode();
	int compare(Record o1, Record o2);

	// functions
	// TODO: which function? from guava, scala (spark)?
	// maybe scala + static method converting scala function to guava function?
	public static class Functions {
		public static Function<Record, Definition> getDefinition() {
			return new Function<Record, Definition>() {
				@Override
				public Definition apply(Record r) {
					return r.getDefinition();
				}
			};
		}

		public static Function<Record, Record> intersect(final Record another) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.intersect(another);
				}
			};
		}

		public static Function<Record, Record> intersect(final Definition def) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.intersect(def);
				}
			};
		}

		public static Function<Record, Record> intersect(final Collection<String> columns) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.intersect(columns);
				}
			};
		}

		public static Function<Record, Record> intersect(final String... columns) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.intersect(columns);
				}
			};
		}

		public static Function<Record, Record> key(final Order order) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.key(order);
				}
			};
		}

		public static Function<Record, Record> minus(final Record another) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.minus(another);
				}
			};
		}

		public static Function<Record, Record> minus(final Definition def) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.minus(def);
				}
			};
		}		public static Function<Record, Record> minus(final Collection<String> columns) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.minus(columns);
				}
			};
		}

		public static Function<Record, Record> minus(final String... columns) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.minus(columns);
				}
			};
		}

		public static Function<Record, Record> concatenate(final Record another) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.concatenate(another);
				}
			};
		}

		public static Function<Record, Record> transform(final Transformation transformation) {
			return transformation;
		}

		public static Function<Record, Record> convertTypes(final Definition def) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.convertTypes(def);
				}
			};
		}

		public static Function<Record, Record> convertTypes(final Column... columns) {
			return new Function<Record, Record>() {
				@Override
				public Record apply(Record r) {
					return r.convertTypes(columns);
				}
			};
		}

		public static Function<Record, SortedMap<String, Pair<Object, Object>>> diff(final Record another) {
			return new Function<Record, SortedMap<String, Pair<Object, Object>>>() {
				@Override
				public SortedMap<String, Pair<Object, Object>> apply(Record r) {
					return r.diff(another);
				}
			};
		}

		public static Function<Record, SortedMap<String, Pair<Class, Class>>> diffDefinition(final Record another) {
			return new Function<Record, SortedMap<String, Pair<Class, Class>>>() {
				@Override
				public SortedMap<String, Pair<Class, Class>> apply(Record r) {
					return r.diffDefinition(another);
				}
			};
		}
	}

	public static class Parsers {
		public static final String DEFAULT_SPLIT = "\t";

		private Parsers() {}

		/**
		 * Parse using passed definition
		 */
		public static Record fromString(final String str, final Definition definition) {
			return CsvParser.parse(str, definition, DEFAULT_SPLIT);
		}

		/**
		 * Parse using passed definition
		 */
		public static Record fromString(final String str, final String split, final Definition definition) {
			return CsvParser.parse(str, definition, split);
		}

		/**
		 * Parse using passed column names - all conversion will be java.lang.String
		 */
		public static Record fromString(final String str, final String... columnNames) {
			Definition definition = Definition.newDefinitionStrings(columnNames);
			return CsvParser.parse(str, definition, DEFAULT_SPLIT);
		}

		/**
		 * Parse using passed column names - all conversion will be java.lang.String
		 */
		public static Record fromString(final String str, final String split, final String... columnNames) {
			Definition definition = Definition.newDefinitionStrings(columnNames);
			return CsvParser.parse(str, definition, split);
		}

		/**
		 * Parse using passed column names - all conversion will be java.lang.String
		 */
		public static Record fromString(final String str, final List<String> columnNames) {
			Definition definition = Definition.newDefinitionStrings(columnNames);
			return CsvParser.parse(str, definition, DEFAULT_SPLIT);
		}

		/**
		 * Parse using passed column names - all conversion will be java.lang.String
		 */
		public static Record fromString(final String str, final String split, final List<String> columnNames) {
			Definition definition = Definition.newDefinitionStrings(columnNames);
			return CsvParser.parse(str, definition, split);
		}

		/**
		 * Parse using definition from database schema
		 */
		public static Record fromRuleSet(final ResultSet rs) {
			Definition definition = definitionFromRuleSet(rs);
			return DbParser.readRecord(rs, definition);
		}

		/**
		 * Parse using definition from database schema but all columns are type of String
		 */
		public static Record fromRuleSetTypedStrings(final ResultSet rs) {
			return DbParser.fromRuleSetTypedStrings(rs);
		}

		/**
		 * Parse using passed definition
		 */
		public static Record fromRuleSet(final ResultSet rs, final Definition definition) {
			return DbParser.readRecord(rs, definition);
		}

		/**
		 * Get definition from database schema
		 */
		public static Definition definitionFromRuleSet(final ResultSet rs) {
			return DbParser.readDefinition(rs);
		}
	}

	// builder
    public static class Builder {
        private final Definition definition;
        private Map<String, Comparable> values = new HashMap<>();

        public Builder(Definition definition) {
            this.definition = definition;
        }

        public Builder set(String colName, Comparable value) {
            values.put(colName, value);
            return this;
        }

        public Record create() {
            return new RecordImpl(new SimpleRecord(definition, values));
        }
    }


}
