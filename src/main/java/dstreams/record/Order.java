package dstreams.record;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.*;

/**
 * Inconsistent with Record.equals. Compare only subset of columns (key)
 */
@Immutable
public class Order implements Comparator<Record>, Serializable {
	private static final long serialVersionUID = 1L;

	private final Map<String, Boolean> order;
    private final List<String> keys;

    private Order(List<String> keys, Map<String, Boolean> order) {
        this.keys = Collections.unmodifiableList(keys);
        this.order = Collections.unmodifiableMap(order);
    }

    public static Order asc(String... keys) {
        Map<String, Boolean> map = new HashMap<>();
        for (String k: keys) {
            map.put(k, true);
        }
        return new Order(Arrays.asList(keys), map);
    }

    // could throw an exception if different conversion
    @Override
    public int compare(Record first, Record second) {
        for (String key: keys) {
            Comparable oFirst = first.getValue(key);
            Comparable oSecond = second.getValue(key);
            int inverse = order.get(key) ? 1 : -1;

            if (oFirst == null) {
                return oSecond == null ? 0 : 1;
            }
            if (oSecond == null) {
                return -1;
            }

            int comp = oFirst.compareTo(oSecond);
            if (comp != 0) {
                return comp * inverse;
            }
        }
        return 0;
    }

    public String keyAsString(Record record) {
        StringBuilder sb = new StringBuilder("[");
        for (String c: keys) {
            sb.append(c).append(": ")
					.append(record.getValue(c)).append(", ");
        }
		// cut tab
		sb.setLength(sb.length() - 2);
		sb.append("]");
        return sb.toString();
    }

    public Set<String> getColumns() {
        return order.keySet();
    }

    public static class Builder {
        private final Map<String, Boolean> order = new HashMap<>();
        private final List<String> keys = new ArrayList<>();

        public Builder keyAsc(String key) {
            keys.add(key);
            order.put(key, Boolean.TRUE);
            return this;
        }

        public Builder keyDesc(String key) {
            keys.add(key);
            order.put(key, Boolean.FALSE);
            return this;
        }

        public Order create() {
            return new Order(keys, order);
        }
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Order[");
		for (String c: keys) {
			String ascDesc = order.get(c) ? "asc" : "desc";
			sb.append(c).append(" ").append(ascDesc).append(", ");
		}
		// cut space
		sb.setLength(sb.length() - 1);
		// replace comma by ]
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}
}
