package dstreams.record;

import com.google.common.base.Function;
import dstreams.conversion.Converter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Transformation implements Serializable, Function<Record, Record> {
	private static final long serialVersionUID = 1L;

	private final Map<String, Converter> converters;
	private final Map<String, String> renamers;
	private final Map<String, Converter<RecordLike, ? extends Comparable>> changers;
	private final Map<String, Converter<RecordLike, ? extends Comparable>> adders;

	public Transformation(
			Map<String, Converter> converters,
			Map<String, String> renamers,
			Map<String, Converter<RecordLike, ? extends Comparable>> changers,
			Map<String, Converter<RecordLike, ? extends Comparable>> adders) {
		this.converters = converters;
		this.renamers = renamers;
		this.changers = changers;
		this.adders = adders;
	}

	public Record transform(Record r) {
        RenamedRecord renamedRecord =
                new RenamedRecord(new ConvertedRecord(r, converters), renamers);

        CalculatedRecord calculatedRecord =
                new CalculatedRecord(renamedRecord, changers, adders);

        return new RecordImpl(calculatedRecord);
	}

	public Function<Record, Record> transformation() {
		return new Function<Record, Record>() {
			@Override
			public Record apply(Record input) {
				return transform(input);
			}
		};
	}

	public Iterator<Record> transform(final Iterator<Record> source) {
		return new Iterator<Record>() {
			@Override
			public boolean hasNext() {
				return source.hasNext();
			}

			@Override
			public Record next() {
				return transform(source.next());
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public Record apply(Record r) {
		return transform(r);
	}

	/**
     * Builder for creating transformation that is able to change dstreams.record by converting its values
     */
    public static class TransformBuilder {
        private final Map<String, Converter> converters = new HashMap<>();
		private final Map<String, String> renamers = new HashMap<>();
		private final Map<String, Converter<RecordLike, ? extends Comparable>> changers = new HashMap<>();
		private final Map<String, Converter<RecordLike, ? extends Comparable>> adders = new HashMap<>();

		public TransformBuilder convert(String column, Converter conv) {
            converters.put(column, conv);
            return this;
        }

		public TransformBuilder convert(String[] column, Converter conv) {
			for (String c: column) {
				converters.put(c, conv);
			}
			return this;
		}

		public TransformBuilder addColumn(String column, Converter<RecordLike, ? extends Comparable> calculation) {
			adders.put(column, calculation);
			return this;
		}

		public TransformBuilder changeValue(String column, Converter<RecordLike, ? extends Comparable> calculation) {
			changers.put(column, calculation);
			return this;
		}

		public TransformBuilder changeColumnName(String column, String newName) {
			renamers.put(column, newName);
			return this;
		}

		public Transformation create() {
			return new Transformation(
					converters,
					renamers,
					changers,
					adders);
		}
    }
}
