package dstreams.record;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.javatuples.Pair;

import java.util.*;

/**
 * Implement convenient API for dealing with record from the base methods (from RecordLike)
 */
public class RecordImpl implements Record {
    private static final long serialVersionUID = 1L;

    private final RecordLike r;

    // lazy cached
    private transient Integer HASH_CODE;

    public RecordImpl(RecordLike record) {
        this.r = record;
    }

    public RecordImpl(Definition definition, Map<String, Comparable> values) {
        r = new SimpleRecord(definition, values);
    }

    public String toString() {
		StringBuilder sb = new StringBuilder("Record[");

		Collection<String> namesUnsorted = r.getDefinition().getColumnNames();
		SortedSet<String> namesSorted = new TreeSet<>(namesUnsorted);

		for (String c: namesSorted) {
			sb.append(c).append(":").append(getValueAsString(r, c)).append(",");
		}
		sb.setCharAt(sb.length()-1, ']');
		return sb.toString();
	}

	public boolean equals(Object o) {
		return o != null
				&& o instanceof RecordLike
				&& equalsWithLogging(r, (RecordLike) o);
	}

    @Override
    public int hashCode() {
        if (HASH_CODE == null) {
            HASH_CODE = Objects.hash(getValues());
        }
        return HASH_CODE;
    }

    @Override
    public int compare(Record o1, Record o2) {
        // TODO: create order as asc all columns and compare
        throw new UnsupportedOperationException();
    }

    private static boolean equalsWithLogging(RecordLike first, RecordLike second) {
		Preconditions.checkArgument(second != null);
		boolean areEquals = equals(first, second);
		if (! areEquals && first.toString().equals(second.toString())) {
			System.err.println("Warn: " + first + " is not " + second);
		}
		return areEquals;
	}

	private static boolean equals(RecordLike first, RecordLike second) {
		Preconditions.checkArgument(second != null);
		if (! first.getDefinition().equals(second.getDefinition())) {
			return false;
		}
		else {
			for (String column: first.getDefinition().getColumnNames()) {
				if (first.getValue(column) == null) {
					if (second.getValue(column) != null) {
						return false;
					}
				}
				else if (! first.getValue(column).equals(second.getValue(column))) {
					return false;
				}
			}
		}
		return true;
	}

	public int compare(Record another) {
		// TODO: create order as asc all columns and compare
		throw new UnsupportedOperationException();
	}

	public Map<String, Comparable> getValues() {
		Map<String, Comparable> result = new HashMap<>();
		for (String col: r.getDefinition().getColumnNames()) {
			result.put(col, r.getValue(col));
		}
		return result;
	}

	private static String getValueAsString(RecordLike r, String columnName) {
		Object value = r.getValue(columnName);
		return value == null ? "" : value.toString();
	}

	public Record intersect(Record another) {
		return this.intersect(another.getDefinition().getColumnNames());
	}

	public Record intersect(Definition def) {
		return new RecordImpl(new SimpleRecord(r.getDefinition().intersect(def.getColumnNames()), this.getValues()));
	}

	public Record intersect(Collection<String> columns) {
		return new RecordImpl(new SimpleRecord(r.getDefinition().intersect(columns), this.getValues()));
	}

	public Record intersect(String... columns) {
		return this.intersect(Lists.newArrayList(columns));
	}

	public Record key(Order order) {
		return this.intersect(order.getColumns());
	}

	public Record minus(Record another) {
		return this.minus(another.getDefinition().getColumnNames());
	}

	public Record minus(Definition def) {
		return this.minus(def.getColumnNames());
	}

	public Record minus(Collection<String> columns) {
		return new RecordImpl(new SimpleRecord(r.getDefinition().minus(columns), this.getValues()));
	}

	public Record minus(String... columns) {
		return this.minus(Lists.newArrayList(columns));
	}

	public Record concatenate(Record another) {
		throw new UnsupportedOperationException();
	}

	public Record transform(Transformation transf) {
		return transf.transform(this);
	}

	// TODO
	public Record convertTypes(Definition def) {
		throw new UnsupportedOperationException();
	}

	// TODO
	public Record convertTypes(Column... columns) {
		throw new UnsupportedOperationException();
	}

	public Record normalize() {
		throw new UnsupportedOperationException();
	}

	public SortedMap<String, Pair<Object, Object>> diff(Record another) {
		SortedMap<String, Pair<Object, Object>> result = new TreeMap();
		if (r.equals(another)) {
			return result;
		}
		else if (! r.getDefinition().equals(another.getDefinition())) {
			throw new IllegalArgumentException(String.format("definitions are different:\r\n%s\r\n%s",
					r.getDefinition(), another.getDefinition()));
		}
		else {
			for (String col: r.getDefinition().getColumnNames()) {
				Comparable firstValue = r.getValue(col);
				Comparable anotherValue = another.getValue(col);
				if (! (firstValue == null && anotherValue == null)
						&& (
						firstValue == null ^ anotherValue == null
								|| ! firstValue.equals(anotherValue))) {
					result.put(col, new Pair(firstValue, anotherValue));
				}
			}
			return result;
		}
	}

	public SortedMap<String, Pair<Class, Class>> diffDefinition(Record another) {
		return r.getDefinition().diff(another.getDefinition());
	}

	@Override
	public String toStringCsv() {
		StringBuilder sb = new StringBuilder();
		for (Column c: getDefinition().getColumns()) {
			sb.append(getValueAsString(r, c.getName())).append("\t");
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	@Override
    public Definition getDefinition() {
        return r.getDefinition();
    }

    @Override
    public <T extends Comparable<T>> T getValue(String s) {
        return r.getValue(s);
    }
}