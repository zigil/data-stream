package dstreams.record;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.*;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

class RenamedRecord implements RecordLike {
	private static final long serialVersionUID = 1L;

	private final RecordLike BaseRecord;
	// <columnName, newName>
	private final BiMap<String, String> renamers;
    private transient Definition renamedDefinition;

	RenamedRecord(RecordLike record, Map<String, String> renamers) {
		this.BaseRecord = record;
		this.renamers = HashBiMap.create(renamers);
        this.renamedDefinition = calculateDefinition(record.getDefinition(), renamers);
	}

	@Override
	public Definition getDefinition() {
		return renamedDefinition;
	}

	private Definition calculateDefinition(Definition def, Map<String, String> renamers) {
		Collection<Column> columns = def.getColumns();
		List<Column> newColumns = new ArrayList<>();
		for (Column c: columns) {
			String newName = renamers.get(c.getName());
			if (newName != null) {
				newColumns.add(column(newName, c.getType()));
			}
            else {
                newColumns.add(c);
            }
		}
		return newDefinition(newColumns);
	}

	@Override
	public <T extends Comparable<T>> T getValue(String s) {
        String oldName = renamers.inverse().get(s);
        if (oldName == null) {
            oldName = s;
        }
		return (T) BaseRecord.getValue(oldName);
	}

}
