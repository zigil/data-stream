package dstreams.record;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.javatuples.Pair;

import java.util.*;

/**
 * Almost Immutable.
 * For performance reason there is no defensive copy for data. This way creating a new records is faster (as data are not copied)
 * Records created by CsvParser and DbReader are immutable.
 * TODO: introduce ImmutableRecord only for inside usage
 */
class SimpleRecord implements RecordLike {
	private static final long serialVersionUID = 1L;

    // may contain entries that doesn't match definition
    private final Map<String, Comparable> data;
    @Getter private final Definition definition;

    public SimpleRecord(Definition definition, Map<String, Comparable> data) {
        this.definition = definition;
        this.data = Collections.unmodifiableMap(data);
        validate();
    }

    @Override
    public <T extends Comparable<T>> T getValue(String s) {
        Preconditions.checkArgument(definition.hasColumn(s), "there is no column "+s);
        return (T) data.get(definition.getColumnName(s));
    }

    private void validate() {
        if (! data.keySet().containsAll(definition.getColumnNames())) {
            Set diff = new HashSet(definition.getColumnNames());
            diff.removeAll(data.keySet());
            throw new IllegalStateException(diff.toString());
        }
    }

}
