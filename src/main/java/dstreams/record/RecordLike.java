package dstreams.record;

import java.io.Serializable;

/**
 * From this API there could introduce full API for Record
 */
public interface RecordLike extends Serializable {
    Definition getDefinition();
    <T extends Comparable<T>> T getValue(String s);
}
