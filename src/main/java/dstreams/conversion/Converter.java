package dstreams.conversion;

import com.google.common.base.Function;


/**
 * Convert a value
 * @param <F>
 * @param <T>
 */
public abstract class Converter<F, T> implements Function<F, T> {
    private Class<T> clazz;

    public Converter(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Class<T> getTargetClass() {
        return clazz;
    }
}
