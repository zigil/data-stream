package dstreams.conversion;

public class ConversionException extends RuntimeException {
	public ConversionException(String msg) {
		super(msg);
	}

	public ConversionException(String msg, RuntimeException e) {
		super(msg, e);
	}
}
