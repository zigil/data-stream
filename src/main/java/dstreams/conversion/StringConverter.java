package dstreams.conversion;

import com.google.common.collect.ImmutableMap;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

/**
 * TODO: quite good implementation - use this as:
 * 		- converting from String into user definition (parsing from String or database)
 * 		- converting into Normalized
 * TODO: configurable other date formats by static methods
 */

/**
 * Convert from String into desire definition.
 * 		Strings are trimmed
 * 		BigDecimals are set scale (eg. -2.00 into -2, 3.1400 into 3.14)
 */
public final class StringConverter {
    private static DateTimeFormatter ORACLE_DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");

	private StringConverter() {}

    public static final Map<Class<? extends Comparable>, Converter<? extends Comparable, ? extends Comparable>> CONVERTERS_FROM_STRING =
		new ImmutableMap.Builder<Class<? extends Comparable>, Converter<? extends Comparable, ? extends Comparable>>()
				.put(Character.class, new Converter<String, Character>(Character.class) {
					@Override
					public Character apply(String input) {
						if (input.length() != 1) {
							throw new ConversionException(String.format("Cannot convert into Character: %s", input));
						}
						return input.charAt(0);
					}
				})
				.put(Byte.class, new Converter<String, Byte>(Byte.class) {
					@Override
					public Byte apply(String input) {
						return Byte.valueOf(input);
					}
				})
				.put(Integer.class, new Converter<String, Integer>(Integer.class) {
					@Override
					public Integer apply(String input) {
						return Integer.valueOf(input);
					}
				})
				.put(Long.class, new Converter<String, Long>(Long.class) {
					@Override
					public Long apply(String input) {
						return Long.valueOf(input);
					}
				})
				.put(Float.class, new Converter<String, Float>(Float.class) {
					@Override
					public Float apply(String input) {
						return Float.valueOf(input);
					}
				})
				.put(Double.class, new Converter<String, Double>(Double.class) {
					@Override
					public Double apply(String input) {
						return Double.valueOf(input);
					}
				})
				.put(BigDecimal.class, new Converter<String, BigDecimal>(BigDecimal.class) {
					@Override
					public BigDecimal apply(String input) {
						BigDecimal bd = new BigDecimal(input).stripTrailingZeros();
						// drop a scale if it's an integer
						if (bd.setScale(0, BigDecimal.ROUND_CEILING).compareTo(bd) == 0) {
							return bd.setScale(0, BigDecimal.ROUND_CEILING);
						} else {
							return bd;
						}
					}
				})
				.put(Timestamp.class, new Converter<String, Timestamp>(Timestamp.class) {
					@Override
					public Timestamp apply(String input) {
						try {
							return new Timestamp(new LocalDate(input.substring(0, 10)).toDate().getTime());
						} catch (IllegalArgumentException e) {
							return new Timestamp(LocalDate.parse(input.substring(0, 10), ORACLE_DATE_FORMAT).toDate().getTime());
						} catch (StringIndexOutOfBoundsException e) {
							return new Timestamp(Long.parseLong(input));
						}
					}
				})
				.put(LocalDate.class, new Converter<String, LocalDate>(LocalDate.class) {
					@Override
					public LocalDate apply(String input) {
						try {
							return new LocalDate(input.substring(0, 10));
						} catch (IllegalArgumentException e) {
							return LocalDate.parse(input.substring(0, 10), ORACLE_DATE_FORMAT);
						}
					}
				})
				.put(java.util.Date.class, new Converter<String, java.util.Date>(java.util.Date.class) {
					@Override
					public java.util.Date apply(String input) {
						try {
							return new LocalDate(input.substring(0, 10)).toDate();
						} catch (IllegalArgumentException e) {
							return LocalDate.parse(input.substring(0, 10), ORACLE_DATE_FORMAT).toDate();
						}
					}
				})
				.put(java.sql.Date.class, new Converter<String, java.sql.Date>(java.sql.Date.class) {
					@Override
					public java.sql.Date apply(String input) {
						try {
							return new java.sql.Date(new LocalDate(input.substring(0, 10)).toDate().getTime());
						} catch (IllegalArgumentException e) {
							return new java.sql.Date(LocalDate.parse(input.substring(0, 10), ORACLE_DATE_FORMAT).toDate().getTime());
						}
					}
				})
				.build();

	/*
	 * there is no way to recognize if empty string should be null or ""
	 * so for simplification of comparison cast this always to null
	 */
	@SuppressWarnings("unchecked")
	public static Object convert(String value, Class<Comparable> type) {
		Object converted;

		if (value == null || value.isEmpty()) {
			converted = null;
		}
		else if (type.equals(String.class)) {
			converted = value.trim();
		}
		else {
			Converter converter = CONVERTERS_FROM_STRING.get(type);
			if (converter == null) {
				throw new ConversionException(String.format("Cannot find converter for value %s, type %s", value, type.getName()));
			}
			try {
				converted = converter.apply(value);
			}
			catch (RuntimeException e) {
				throw new ConversionException(String.format("Problem convertering value %s, type %s", value, type.getName()), e);
			}
		}

		return converted;
	}

}
