package dstreams.comparator;

import lombok.Getter;
import dstreams.record.Definition;
import dstreams.record.Order;
import dstreams.record.Record;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class Comparator {
    private static enum LOAD {
        BOTH, FIRST, SECOND;
        public boolean first() {return this.equals(BOTH) || this.equals(FIRST);}
        public boolean second() {return this.equals(BOTH) || this.equals(SECOND);}
    }

    private Iterator<Record> firstStream;
    private Iterator<Record> secondStream;

    private final Order keyOrder;
    private final Set<String> toSkip = new HashSet<>();
    @Getter private final ComparisonResult comparisonResult;

    public Comparator(String outPath, Iterator<Record> first, Iterator<Record> second, Order order) throws FileNotFoundException {
        this.firstStream = first;
        this.secondStream = second;
        this.keyOrder = order;
        this.comparisonResult = new ComparisonResult(outPath, order);
    }

    public Comparator(String outPath, Iterable<Record> first, Iterable<Record> second, Order order) throws FileNotFoundException {
        this.firstStream = first.iterator();
        this.secondStream = second.iterator();
        this.keyOrder = order;
        this.comparisonResult = new ComparisonResult(outPath, order);
    }

	public boolean compareSchemas(Record first, Record second) {
        Definition firstDef = first.getDefinition();
        Definition secondDef = second.getDefinition();

		if (firstDef.equals(secondDef)) {
            comparisonResult.schemasTheSame(first.getDefinition());
			return true;
        }
        else {
            comparisonResult.differenceInSchema(firstDef, secondDef);
            return false;
        }
    }

    public boolean compare() {
        if (detectEmptyStreams()) return false;

        OrderGuard firstOrderGuard = new OrderGuard(keyOrder);
        OrderGuard secondOrderGuard = new OrderGuard(keyOrder);

        Record first = nextFromStream(firstStream, firstOrderGuard);
        Record second = nextFromStream(secondStream, secondOrderGuard);

        if (! compareSchemas(first, second)) {
            return false;
        }

        LOAD toLoad;
        do {
            if (first == null) {
                comparisonResult.oneOfStreamsIsEmpty(second, 1);
                toLoad = LOAD.SECOND;
            }
            else if (second == null) {
                comparisonResult.oneOfStreamsIsEmpty(first, 2);
                toLoad = LOAD.FIRST;
            }
            else {
                int compareResult = keyOrder.compare(first, second);
                if (compareResult == 0) {
                    if (first.equals(second)) {
                        comparisonResult.recordsAreTheSame(first, second);
                    }
                    else {
                        comparisonResult.recordsHasSameKeysButDiff(first, second);
                    }
                    toLoad = LOAD.BOTH;
                }
                else if (compareResult < 0) {
                    comparisonResult.extraRecord(first, 1);
                    toLoad = LOAD.FIRST;
                }
                else {
                    comparisonResult.extraRecord(second, 2);
                    toLoad = LOAD.SECOND;
                }
            }

            if (toLoad.first()) {
                first = nextFromStream(firstStream, firstOrderGuard);
            }
            if (toLoad.second()) {
                second = nextFromStream(secondStream, secondOrderGuard);
            }
        }
        while (first != null || second != null);

        comparisonResult.summary();

        return comparisonResult.result();
    }

    private Record nextFromStream(Iterator<Record> stream, OrderGuard orderGuard) {
        if (! stream.hasNext()) {
            return null;
        }
        Record next = stream.next();
        orderGuard.validateNext(next);
        return next.minus(toSkip);
    }

    private boolean detectEmptyStreams() {
        if (! firstStream.hasNext()
                && secondStream.hasNext()) {
            comparisonResult.oneOfStreamsIsEmpty(secondStream.next(), 1);
            return true;
        }
        else if (firstStream.hasNext()
                && ! secondStream.hasNext()) {
            comparisonResult.oneOfStreamsIsEmpty(firstStream.next(), 2);
            return true;
        }
        else if (! firstStream.hasNext()
                && ! secondStream.hasNext()) {
            comparisonResult.bothStreamsAreEmpty();
            return true;
        }
        return false;
    }

    private class ComparisonResult {
        private boolean result = true;
        private Map<String, Boolean> logged = new HashMap<>();
        private int countSame;
        private int countDiff;
        private long[] countExtraRecords = new long[]{0l, 0l};

        public boolean result() {
            return result;
        }

        private ExtraRecordsReporter[] reportersExtraRecords;
        private DiffReporter reporterDiff;
        private PrintStream summary;

		private Definition definition;
		private final Order order;

        public ComparisonResult(String basePath, Order order) throws FileNotFoundException {
			this.order = order;
			if (! basePath.endsWith("/")) {
                basePath += "/";
            }
            reportersExtraRecords = new ExtraRecordsReporter[]{
                    new ExtraRecordsReporter(basePath + "only_in_first.txt"),
                    new ExtraRecordsReporter(basePath + "only_in_second.txt")};
            reporterDiff = new DiffReporter(basePath + "diff.txt");
            summary = new PrintStream(basePath + "summary.txt");
        }

        public void recordsAreTheSame(Record first, Record second) {
            countSame++;
            reporterDiff.recordsAreTheSame(first, second);
        }

        public void extraRecord(Record record, int stream) {
            countExtraRecords[stream-1]++;
            result = false;
            reportersExtraRecords[stream-1].extraRecordFound(keyOrder.keyAsString(record), record);
        }

        public void recordsHasSameKeysButDiff(Record first, Record second) {
            countDiff++;
            result = false;
            reporterDiff.recordDiffer(first, second, keyOrder);
        }

        public void oneOfStreamsIsEmpty(Record record, int i) {
            String key = "oneOfStreamsIsEmpty"+i;
            countExtraRecords[2-i]++;
            reportersExtraRecords[2-i].extraRecordFound(keyOrder.keyAsString(record), record);
            if (logged.get(key) == null) {
                String prefix = i==1 ? "First": "Second";
                summary.println(prefix + " of a given stream is totally empty");
                result = false;
                logged.put(key, Boolean.TRUE);
            }
        }

        public void differenceInSchema(Definition firstDef, Definition secondDef) {
            summary.println("Differences in schemas:");
            summary.println("First minus second: " + firstDef.minus(secondDef));
            summary.println("Second minus first: " + secondDef.minus(firstDef));
        }

        public void schemasTheSame(Definition definition) {
			this.definition = definition;
            summary.println("Schemas are the same");
        }

        public void bothStreamsAreEmpty() {
            summary.println("Both of given streams are totally empty");
        }

        public void summary() {
            long all = countSame + countDiff + countExtraRecords[0] + countExtraRecords[1];
            _summary(all, summary);
            _summary(all, System.out);
        }

        public void _summary(long countAll, PrintStream summary) {
			if (definition != null) {
				summary.println("--------------------------------------");
				summary.println("Definition:");
				summary.println(Arrays.toString(definition.getColumns().toArray()));
				summary.println("Order:");
				summary.println(order.toString());
			}
            summary.println("--------------------------------------");
            summary.println("First stream size:  " + (countSame + countDiff + countExtraRecords[0]));
            summary.println("Second stream size: " + (countSame + countDiff + countExtraRecords[1]));

            summary.println("--------------------------------------");
            summary.println("Number of comparison: " + countAll);
            summary.println("Records equal: " + countSame);
            summary.println("Records diff:  " + countDiff);
            summary.println("Records not found in the first stream:  " + countExtraRecords[1]);
            summary.println("Records not found in the second stream: " + countExtraRecords[0]);

            summary.println("--------------------------------------");
            summary.println("Compatibility " + (countSame * 100 / countAll));

            if (result) {
                summary.println("Streams are THE SAME");
            }
            else {
                summary.println("Streams are NOT the same");
            }
        }
    }
}
