package dstreams.comparator;

import dstreams.record.Record;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class ExtraRecordsReporter {
    private PrintStream out;

    public ExtraRecordsReporter(String s) throws FileNotFoundException {
        out = new PrintStream(s);
		out.println("Format: first line is a key, second line is a whole record");
		out.println("-------------------------------");
    }

    public void extraRecordFound(String key, Record record) {
        out.println(key);
		out.println(record.toStringCsv());
        out.println("");
    }
}