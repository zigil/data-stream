package dstreams.comparator;

import dstreams.record.Order;
import dstreams.record.Record;

public class OrderGuard {
    private final Order order;
    private Record lastRecord;

    public OrderGuard(Order order) {
        this.order = order;
    }

    public void validateNext(Record record) {
        if (lastRecord != null
                && order.compare(lastRecord, record) > 0) {
            throw new RuntimeException("Stream is not sorted:\r\n"+lastRecord+"\r\n"+record);
        }
        lastRecord = record;
    }
}
