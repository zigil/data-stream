package dstreams.comparator;

import org.javatuples.Pair;
import dstreams.record.Order;
import dstreams.record.Record;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map;
import java.util.SortedMap;

public class DiffReporter {
    private PrintStream out;

    public DiffReporter(String path) throws FileNotFoundException {
        out = new PrintStream(path);
    }

    public void recordDiffer(Record first, Record second, Order keyOrder) {
        out.println("diff for the key: " + keyOrder.keyAsString(first));

        SortedMap<String, Pair<Object, Object>> diff = first.diff(second);
        String cols = "";
        String firstOut = "";
        String secondOut = "";
        for (Map.Entry<String, Pair<Object, Object>> e: diff.entrySet()) {
            cols += e.getKey() + "\t";
            firstOut += e.getValue().getValue0() + "\t";
            secondOut += e.getValue().getValue1() + "\t";
        }

        out.println(cols);
        out.println(firstOut);
        out.println(secondOut);
        out.println("");
    }

    public void recordsAreTheSame(Record first, Record second) {
        // do nothing
    }
}
