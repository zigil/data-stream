package dstreams.streams;

import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

// TODO: implement files systems to deal with Hdfs
public final class Source {

    private final Iterable<String> lines;

    private Source(final Iterable<String> lines) {
        this.lines = lines;
    }

    public static Source fromWrapped(final Iterable<String> lines) {
        return new Source(lines);
    }

    public static Source fromClassPath(final String resource) {
        return fromStream(Source.class.getClassLoader().getResourceAsStream(resource));
    }

    public static Source fromStream(final InputStream is) {
        return fromBuffer(new BufferedReader(new InputStreamReader(is)));
    }

    public static Source fromBuffer(final BufferedReader br) {
        return new Source(linesIterable(br));
    }

    public static Source fromString(final String csv) {
        return fromBuffer(new BufferedReader(new StringReader(csv)));
    }

    public static Source fromPath(String path) throws FileNotFoundException {
        return fromBuffer(new BufferedReader(new FileReader(path)));
    }

    public Iterable<String> getLines() {
        return lines;
    }

    private static Iterable<String> linesIterable(final BufferedReader br) {
        return new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                return new Iterator<String>() {
                    String line = null;

                    @Override
                    public boolean hasNext() {
                        if (line == null) {
                            try {
                                line = br.readLine();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        return line != null;
                    }

                    @Override
                    public String next() {
                        hasNext();
                        if (line == null) {
                            throw new NoSuchElementException();
                        }
                        String copy = line;
                        line = null;
                        return copy;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

}
