package dstreams.streams;

import dstreams.conversion.StringConverter;
import dstreams.record.*;

import java.sql.*;
import java.util.*;

import static dstreams.record.Column.column;
import static dstreams.record.Definition.newDefinition;

/**
 * Read from database a Definition or Iterable of Records.
 * Can execute any sql-query.
 * Loading of data is lazy.
 */
public final class DbParser {
	private DbParser() {};

	/**
	 * Read definition from database schema
	 */
	public static Definition readDefinition(ResultSet rs) {
		try {
			ResultSetMetaData md = rs.getMetaData();
			int columnsCount = md.getColumnCount();
			List<Column> columns = new ArrayList<>();
			String[] orderedColumns = new String[columnsCount];

			for (int i=1; i<=columnsCount; i++) {
				String label = md.getColumnLabel(i).toLowerCase();
				String className = md.getColumnClassName(i);
				try {
					columns.add(column(label, Class.forName(className)));
					orderedColumns[i-1] = label;
				} catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
			return newDefinition(columns);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
    }

	/**
	 * Read record from ResultSet. ResultSet should be active (rs.next())
	 * Definition will be taken from database schema
	 */
	public static Record readRecord(final ResultSet rs) {
		Definition def = readDefinition(rs);
		return readRecord(rs, def);
	}

	/**
	 * Parse using definition from database schema but all columns are type of String
	 */
	public static Record fromRuleSetTypedStrings(final ResultSet rs) {
		Definition definition = readDefinition(rs);
		return DbParser.readRecord(rs, definition.overrideTypesToString());
	}

	/**
	 * Read record from ResultSet. ResultSet should be active (rs.next())
	 */
	public static Record readRecord(final ResultSet rs, final Definition definition) {
		Map<String, Comparable> values = new HashMap<>();
		for (Column c: definition.getColumns()) {

			String value;
			try {
				value = rs.getString(c.getName());
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}

			values.put(c.getName(), (Comparable) StringConverter.convert(value, c.getType()));
		}
		return new RecordImpl(definition, values);
	}

	/**
	 * Read records from ResultSet.
	 * Definition will be taken from database schema
	 */
	public static Iterable<Record> readAllRecords(final ResultSet rs) throws SQLException {
		Definition def = readDefinition(rs);
		return readAllRecords(rs, def);
	}

	/**
	 * Read records from ResultSet.
	 * Definition will be taken from database schema
	 */
	public static Iterable<Record> readAllRecordsTypedStrings(final ResultSet rs) throws SQLException {
		Definition def = readDefinition(rs);
		return readAllRecords(rs, def.overrideTypesToString());
	}

	/**
	 * Read records from ResultSet.
	 */
	public static Iterable<Record> readAllRecords(final ResultSet rs, final Definition definition) throws SQLException {
		return new ResultSet2Iterator(rs, definition);
	}

	private static class ResultSet2Iterator implements Iterable<Record> {
        private final ResultSet rs;
        private final Definition definition;

        public ResultSet2Iterator(ResultSet rs, Definition definition) throws SQLException {
            this.rs = rs;
            this.definition = definition;
        }

        @Override
        public Iterator<Record> iterator() {
            return new Iterator<Record>() {
                boolean shouldReadNext = true;
                // null - not read
                boolean hasNext;

                @Override
                public boolean hasNext() {
                    if (shouldReadNext) {
                        shouldReadNext = false;
                        try {
                            return hasNext = rs.next();
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    else {
                        return hasNext;
                    }
                }

                @Override
                public Record next() {
                    if (! hasNext()) {
                        throw new NoSuchElementException();
                    }
                    else {
                        shouldReadNext = true;
						return readRecord(rs, definition);
                    }
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }
	}
}
