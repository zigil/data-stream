package dstreams.streams;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import dstreams.conversion.ConversionException;
import dstreams.record.*;
import dstreams.conversion.StringConverter;

import java.util.*;

/**
 * Parse a source into Iterable of Records.
 * Can sort but be careful as this is operation in memory
 */
public final class CsvParser {
	private CsvParser() {}

	public static Record parse(final String str, final Definition definition, final String split) {
		return toRecord(readColumns(str, split), definition);
	}

	/**
	 * Be careful: in-memory load and sort
	 */
	public static Iterable<Record> parseAllAndSort(final Source source, final Definition definition, final String split, final Order order) {
		Iterable<Record> records = parseAll(source, definition, split);
		List<Record> asList = Lists.newArrayList(records);
		Collections.sort(asList, order);
		return asList;
	}

	public static Iterable<Record> parseAll(final Source source, final Definition definition, final String split) {
		return FluentIterable.from(source.getLines())
				.filter(new Predicate<String>() {
					@Override
					public boolean apply(String lineCandidate) {
						return !lineCandidate.trim().isEmpty();
					}
				})
				.transform(new Function<String, Record>() {
					@Override
					public Record apply(String line) {
						return parse(line, definition, split);
					}
				});
	}

	private static String[] readColumns(String line, String split) {
		return Iterables.toArray(Splitter.on(split).split(line), String.class);
	}

	private static Record toRecord(String[] data, Definition definition) {
		Preconditions.checkArgument(data.length >= definition.getColumns().size(),
				String.format("Not enough data to initialize dstreams.record %d : %d\r\ndstreams.record: %s\r\ndefinition: %s",
						data.length, definition.getColumns().size(), Arrays.toString(data), definition));

		Map<String, Comparable> values = new HashMap<>();
		int i = 0;
		for (Column c: definition.getColumns()) {
			String value = data[i++];
			try {
				values.put(c.getName(), (Comparable) StringConverter.convert(value, c.getType()));
			} catch (ConversionException e) {
				throw new ConversionException("Cannot read column " + c.getName(), e);
			}
		}
		return new RecordImpl(definition, values);
	}


}
